
//Найти среднее значение суммы чисел от 1 до 100

import java.text.DecimalFormat;

public class Task4 {
    public static void main (String [] args) {
        double sum = 0;
        for (int i =1; i<=100; i ++) {
            sum+=i;
        }
        double res = sum/100;
        System.out.print("Result: "+res);
    }
}
