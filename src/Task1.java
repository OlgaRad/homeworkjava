import java.util.Scanner;


/* Необходимо написать программу, где бы пользователю предлагалось ввести число на выбор:
      1, 2 или 3, а программа должна сказать, какое число ввёл пользователь: 1, 2, или 3.
      Написать двумя способами. If и switch.
      */
      /*System.out.println("Введите число 1, 2 или 3: ");
      Scanner inputFigure = new Scanner(System.in);
      int i = inputFigure.nextInt();
      System.out.println(i);*/

public class Task1 {
    public static void main (String[] args) {
//        via if
//        int number;
//        do {
//            System.out.println("Введите число 1, 2 или 3:  ");
//            Scanner inputFigure = new Scanner(System.in);
//            number = inputFigure.nextInt();
//            if (number == 1) {
//                System.out.println("You enter: " + number);
//            } else if (number == 2) {
//                System.out.println("You enter: " + number);
//            } else if (number == 3) {
//                System.out.println("You enter: " + number);
//            } else {
//                System.out.println("You enter wrong number!!!");
//            }
//        }while (number!=4);
//        System.out.println("Buy!!!");

//        via switch
        int number;
        do {
            System.out.println("Введите число 1, 2 или 3:  ");
            Scanner inputFigure = new Scanner(System.in);
            number = inputFigure.nextInt();

            switch (number) {
                case 1:
                    System.out.println("You enter: " + number);
                    break;
                case 2:
                    System.out.println("You enter : " + number);
                    break;
                case 3:
                    System.out.println("You enter  : " + number);
                    break;
                default:
                    System.out.println("You enter wrong number!!!");
            }
        } while (number != 4) ;
        System.out.println("Buy!!!");
    }
}




