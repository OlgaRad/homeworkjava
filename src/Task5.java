
//Найти максимальное число в массиве int [] array = {5,2,4,8,88,22,10}

public class Task5 {
    public static void main(String[] args) {
        int [] array = {5,2,4,8,88,22,10};
        int maxNumber = array[0];
        for (int i=1; i<array.length; i++) {
            if(array[i]>maxNumber) {
                maxNumber = array[i];
            }
        }
        System.out.println("MaxNumber: "+ maxNumber);

    }
}
